package fortumo.demo.aggregate;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class JsonDeliveryService implements DisposableBean {

    private final JsonStoreLru store;

    public JsonDeliveryService(long pauseBetweenAttempts, long timeout, TimeUnit timeUnit) {
        store = new JsonStoreLru(pauseBetweenAttempts, timeout, timeUnit);
    }

    @RabbitListener(queues = "${instanceName}")
    public void add(@Payload String fooJson, @Header String msgId) {
        store.add(msgId, fooJson);
    }

    public String remove(String id) {
        return store.syncRemove(id);
    }

    @Override
    public void destroy() throws Exception {
        store.terminate();
    }
}
