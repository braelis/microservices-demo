package fortumo.demo.aggregate;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AggregateController {

    private final WebAskService webAskService;
    private final JsonDeliveryService jsonDeliveryService;

    private final String instanceName;

    public AggregateController(WebAskService webAskService, JsonDeliveryService jsonDeliveryService, String instanceName) {
        this.webAskService = webAskService;
        this.jsonDeliveryService = jsonDeliveryService;

        this.instanceName = instanceName;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/aggregate", produces = MediaType.APPLICATION_JSON_VALUE)
    public String aggregate() {
        String id = webAskService.ask(instanceName);
        return jsonDeliveryService.remove(id);
    }
}
