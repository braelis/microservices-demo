package fortumo.demo.aggregate;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.concurrent.TimeUnit;

@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
// Disable component scanner ...
@ComponentScan(useDefaultFilters = false)

public class AggregateServer {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "aggregate-server");
        SpringApplication.run(AggregateServer.class, args);
    }

    @Value("${instanceName}")
    private String instanceName;
    @Value("${pauseBetweenAttempts}")
    private long pauseBetweenAttempts;
    @Value("${timeout}")
    private long timeout;
    @Value("${timeUnit}")
    private TimeUnit timeUnit;

    @Bean
    public Queue queue() {
        return new Queue(instanceName, false, false, true);
    }

    @Bean
    public WebAskService askService() {
        return new WebAskService();
    }

    @Bean
    public JsonDeliveryService fooDeliveryService() {
        return new JsonDeliveryService(pauseBetweenAttempts, timeout, timeUnit);
    }

    @Bean
    public AggregateController aggregatorController() {
        return new AggregateController(askService(), fooDeliveryService(), instanceName);
    }

}
