package fortumo.demo.aggregate;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;

import java.util.concurrent.*;

public class JsonStoreLru {

    private final int maxThreads = 20;
    private final long maxCapacity = 200;

    private final ConcurrentLinkedHashMap<String, String> store;
    private final long pauseBetweenAttempts;
    private final long timeout;
    private final TimeUnit timeUnit;

    private final ExecutorService executor;

    public JsonStoreLru(long pauseBetweenAttempts, long timeout, TimeUnit timeUnit) {
        store = new ConcurrentLinkedHashMap.Builder<String, String>()
                .maximumWeightedCapacity(maxCapacity)
                .concurrencyLevel(maxThreads)
                .build();
        executor = Executors.newFixedThreadPool(maxThreads);

        this.pauseBetweenAttempts = pauseBetweenAttempts;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    public void add(String key, String fooJson) {
        store.put(key, fooJson);
    }

    public String syncRemove(final String key) throws AggregateException {
        Future<String> fooTask;

        try {
            fooTask = executor.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    String fooJson;
                    while (true) {
                        fooJson = store.remove(key);
                        if (fooJson == null) {
                            timeUnit.sleep(pauseBetweenAttempts);
                        } else {
                            return fooJson;
                        }
                    }
                }
            });
        } catch (RejectedExecutionException e) {
            throw new AggregateException(e);
        }

        try {
            return fooTask.get(timeout, timeUnit);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new AggregateException(e);
        }
    }

    public void terminate() {
        store.setCapacity(0);
        executor.shutdown();
        try {
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
