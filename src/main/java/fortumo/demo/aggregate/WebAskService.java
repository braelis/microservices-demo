package fortumo.demo.aggregate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Service
public class WebAskService {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${askServiceUrl}")
    private String askServiceUrl;

    public String ask(final String instanceName) throws AggregateException {
        final String msgId = UUID.randomUUID().toString();
        try {
            URI askUri = UriComponentsBuilder.fromUriString(askServiceUrl)
                    .queryParam("instanceName", instanceName)
                    .queryParam("msgId", msgId)
                    .build().toUri();

            restTemplate.getForObject(askUri, Void.class);
        } catch (RestClientException | IllegalStateException e) {
            throw new AggregateException(e);
        }
        return msgId;
    }
}
