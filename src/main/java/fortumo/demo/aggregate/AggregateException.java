package fortumo.demo.aggregate;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class AggregateException extends RuntimeException {

	private static final long serialVersionUID = 2L;

	public AggregateException(Exception e) {
		super(e);
	}
}
