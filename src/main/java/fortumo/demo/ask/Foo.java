package fortumo.demo.ask;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Foo implements Serializable {

    private static final long serialVersionUID = 545698194652926338L;

    public Foo() {
        timestamp = new DateTime(DateTimeZone.UTC).toString();
        response = new HashMap<>();
        response.put("some", "value");
    }

    public String timestamp;
    public Map<String, String> response;
}
