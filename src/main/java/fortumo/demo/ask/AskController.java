package fortumo.demo.ask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
public class AskController {

    private final Logger log = Logger.getLogger(AskController.class.getName());

    @Autowired
    private PingService askFooService;

    @RequestMapping(method = RequestMethod.GET, value = "/ask", produces = MediaType.APPLICATION_JSON_VALUE)
    public String ask(@RequestParam String instanceName, @RequestParam String msgId) {
        log.info("Initiating the async ping to " + instanceName + "." + msgId);
        askFooService.asyncPing(instanceName, msgId);
        return null;
    }
}
