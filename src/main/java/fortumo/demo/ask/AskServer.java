package fortumo.demo.ask;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@EnableAutoConfiguration
@EnableDiscoveryClient
public class AskServer {


    @Value("${callbackServiceUrl}")
    public String callbackServiceUrl;

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "ask-server");
        SpringApplication.run(AskServer.class, args);
    }

    @Bean
    public PingService pingService() {
        return new PingService(callbackServiceUrl);
    }

    @Bean
    public AskController askController() {
        return new AskController();
    }

//    @Bean
//    public Queue queue() {
//        return new Queue(instanceName, false, false, true);
//    }
//
//    @Bean
//    public WebAskService askService() {
//        return new WebAskService(CALLBACK_SERVICE_URL);
//    }
//
//    @Bean
//    public RetrieveService retrieveService() {
//        return new RetrieveService();
//    }
}
