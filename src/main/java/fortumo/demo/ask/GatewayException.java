package fortumo.demo.ask;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class GatewayException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public GatewayException(Exception e) {
		super(e);
	}

}
