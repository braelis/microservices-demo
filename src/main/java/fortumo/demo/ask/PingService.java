package fortumo.demo.ask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PingService {

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = Logger.getLogger(PingService.class.getName());

    private final String callbackServiceUrl;
    private final int maxThreads = 20;
    private final int delaySec = 5;
    private final ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(maxThreads);

    public PingService(String callbackServiceUrl) {
        this.callbackServiceUrl = callbackServiceUrl;
    }

    public void asyncPing(final String instanceName, final String msgId) throws GatewayException {
        try {
            log.info("waiting before pinging " + instanceName + "." + msgId);
            scheduledThreadPool.schedule(new Runnable() {
                @Override
                public void run() {
                    log.info("Pinging " + instanceName + "." + msgId);
                    try {
                        URI callbackServiceUri = UriComponentsBuilder.fromUriString(callbackServiceUrl)
                                .queryParam("instanceName", instanceName)
                                .queryParam("msgId", msgId)
                                .build().toUri();

                        restTemplate.postForObject(callbackServiceUri, new Foo(), Void.class);
                        log.info("Pinged " + instanceName + "." + msgId);
                    } catch (RestClientException | IllegalStateException e) {
                        log.info("Unable to ping " + instanceName + "." + msgId);
                    } catch (Exception e) {
                        log.log(Level.SEVERE, "Something went wrong", e);
                    }
                }
            }, delaySec, TimeUnit.SECONDS);
        } catch (RejectedExecutionException e) {
            log.info("No executors available!");
            throw new GatewayException(e);
        }
    }
}
