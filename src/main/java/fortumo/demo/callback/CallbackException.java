package fortumo.demo.callback;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class CallbackException extends RuntimeException {

	private static final long serialVersionUID = 3L;

	public CallbackException(Exception e) {
		super(e);
	}
}
