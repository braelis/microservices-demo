package fortumo.demo.callback;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class CallbackController {

    private final SubmittingService submittingService;

    public CallbackController(SubmittingService submittingService) {
        this.submittingService = submittingService;
    }

    @RequestMapping(name = "/callback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String callback(@RequestParam String instanceName, @RequestParam String msgId, @RequestBody String fooJson) {
        submittingService.submit(instanceName, msgId, fooJson);
        return null;
    }
}
