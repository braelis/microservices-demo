package fortumo.demo.callback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
// Disable component scanner ...
@ComponentScan(useDefaultFilters = false)

public class CallbackServer {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "callback-server");
        SpringApplication.run(CallbackServer.class, args);
    }

    @Bean
    public SubmittingService fooSubmittingService() {
        return new SubmittingService();
    }

    @Bean
    public CallbackController callbackController() {
        return new CallbackController(fooSubmittingService());
    }
}
