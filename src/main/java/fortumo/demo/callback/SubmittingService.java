package fortumo.demo.callback;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubmittingService {

    @Autowired
    private AmqpTemplate template;

    public void submit(String instanceName, final String msgId, String fooJson) throws CallbackException {
        try {
            template.convertAndSend(instanceName, (Object)fooJson, new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    message.getMessageProperties().setHeader("msgId", msgId);
                    return message;
                }
            });
        } catch (AmqpException e) {
            throw new CallbackException(e);
        }
    }
}
