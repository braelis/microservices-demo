# microservices-demo

Install and launch local RabbitMQ with default configuration. Default broker configurations can be found in rabbit_mq_broker_definitions.json

If a custom config is in use, update the *.yml configs correspondingly

## Execution

### Using an IDE

You can run the system in your IDE by running the four servers in order:

_RegistrationServer_, _AggregateServer_ , _AskServer_ and _CallbackServer_.

Open the Eureka dashboard [http://localhost:1111](http://localhost:1111) in your browser to see that three applications have registered.

Will take about 30-40 sec until microservices retrieve all the necessary routing from Eureka, until that error 503 is thrown.

Next open the Demo Home Page [http://localhost:2222/aggregate](http://localhost:2222/aggregate), which results in JSON request (or wait until 503 goes away if still displayed)

## Short overview

### Scope: Java 7, Spring Boot (on Embedded Jetty), Spring Cloud (Eureka), Amqp (RabbitMQ)

### Workflow

Can be 1 instance of Eureka (_RegistrationServer_), 1 instance (can be clustered) of RabbitMQ, K instances of AggregateServer,
L instances of Ask Server and M instances of CallbackServer, where K, L, M >= 1

Each AggregateServer holds unique instanceName and listens to RabbitMQ bus using this instanceName as a queue name. If there are
many AggregateServer instances, they all use own queues on the same RabbitMQ. When AggregateServer instance is connected via
HTTP GET, it sends pair (instanceName, unique msgId) to one of AskServer instances randomly chosen by Eureka and asks AskServer
to launch an async operation. AskServer closes connection, waits a few seconds, produces JSON and sends it to a random CallbackServer
instance via HTTP POST, the pair (instanceName, msgId) is sent as well. CallbackServer puts JSON into a queue defined by instanceName
with msgId as a header. AggregateServer listens to his queue until receives the answer or until timeout. The result is returned to the
client and AggregateServer finally closes the session.

RabbitMQ queues require fine tuning, currently queues are just marked to be automatically deleted, no timeouts, filters, etc.

Resources of AggregateServer and AskServer are limited (time, executors etc), can be configured mainly via *.yml files.

### Points of interest

JsonStoreLru - stores JSON records and corresponding msgId in LRU-like cache - acts as a buffer for JSON messages from RabbitMQ.
JSON retrieval by msgId involves waiting if currently there is no corresponding msgId in the buffer.

PingService - executes delayed REST request

*.yml - url part - instead of host names and ports used microservice name in uppercase.

pom.xml - Joda-time, Google concurrentlinkedhashmap-lru (can be substituted with Trove collections or other solutions)